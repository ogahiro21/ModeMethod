#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define SIZE 256

// mode_method
void mode_method(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                 Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH]);

void create_hist(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                 int histogram[]);

// binarization
void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int threashold);

// Search for the maximum pixel value
int search_maxi(int histogram[], int peak);

int search_valley(Uchar source[COLORNUM][MAXHEIGHT][MAXHEIGHT],
                  int histogram[], int peak, int secPeak);

int main(int argc, char *argv[])
{
  imgdata idata;

  if (argc < 3) {
    printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  }

  else {
    if (readBMPfile(argv[1], &idata) > 0){
      printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    }
    else {
      // mode_method
      mode_method(idata.source, idata.results);
    }
    if (writeBMPfile(argv[2], &idata) > 0){
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void mode_method(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                 Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH])
{
  int peak; // Store maximum value
  int secondPeak;
  int threashold;
  int histogram[SIZE];

  create_hist(source, histogram);

  peak       = search_maxi(histogram,  305); //255 + 50
  secondPeak = search_maxi(histogram, peak);
  threashold  = search_valley(source, histogram, peak, secondPeak);
  printf("threashold : %d\n", threashold);
  binarization(source, results, threashold);


}

void create_hist(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                 int histogram[])
{
  int x,y;

  // Initialize histogram[]
  for (x = 0; x < SIZE; x++) {
    histogram[x] = 0;
  }

  // create a histogram
  for (y = 0; y < SIZE; y++) {
    for (x = 0; x < SIZE; x++) {
      if (source[RED][y][x] == 0) {
        printf("(%d, %d)\n",y,x );
      }
      histogram[source[RED][y][x]] += 1;
    }
  }
}

int search_maxi(int histogram[], int peak)
{
  int i; // for
  int max = 1;

  for (i = 0; i < SIZE; i++) {
      if(histogram[i] > histogram[max]){
        if(abs(peak - i) > 50) {
          max = i;
        }
      }
  }
  printf("peak %d\n", max);
  return max;
}


int search_valley(Uchar source[COLORNUM][MAXHEIGHT][MAXHEIGHT],
                  int histogram[], int peak, int secPeak)
{
  int i;
  int crement = 1;
  int threashold = peak;

  if(peak > secPeak) crement = -1;
  for (i = peak; i < secPeak; i += crement) {
    if (histogram[threashold] > histogram[i]) threashold = i;
  }
  return threashold;
}

void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int threashold)
{
  int x, y, color; // for
  for (color = 0; color < 3; color++) {
    for (y = 0; y < MAXHEIGHT; y++) {
      for (x = 0; x < MAXWIDTH; x++) {
        if(source[color][y][x] >= threashold) results[color][y][x] = 255;
        else results[color][y][x] = 0;
      }
    }
  }
}
