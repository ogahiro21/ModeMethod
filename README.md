# Mode Method
モード法により閾値を決めて、入力画像を２値化する。

## Description   
谷を探す時の範囲は、ヒストグラムの **最大ピーク値の濃度値から２番目のピーク値の濃度値の範囲とする**。  
**2番目のピーク値は、最大ピーク値の濃度値よりも５０離れた濃度値の範囲** で探す。  
- 入力画像  
![in](https://gitlab.com/ogahiro21/ModeMethod/raw/image/image/in16.jpeg)  
- 出力画像  
![out](https://gitlab.com/ogahiro21/ModeMethod/raw/image/image/ans16.jpeg)

## Usage
**コンパイル**
```
gcc -o cpbmp mode_method.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in16.bmp ans16.bmp
```
**正解画像と回答画像の比較**
```
/home/class/j5/imageSystem/bin/diffbmp out16.bmp ans16.bmp
```
